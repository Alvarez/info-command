package me.alvarez;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;


public class InfoCommand extends JavaPlugin implements Listener{
	public static InfoCommand plugin;
	
	public String[] commands;
	public static Map<String, String[]> cmdResp = new HashMap<String, String[]>();

	@Override
	public void onEnable(){
		System.out.println("<<Info Command>> by Alvarez");
		getServer().getPluginManager().registerEvents(this, this);
		
		plugin = this;
		
		this.saveDefaultConfig();
		
		getCmds();
		
		checkFiles();
		
		System.out.println("<<Info Command>> Commands Successfully found!");
		

		System.out.println("<<Info Command>> Version " + this.getDescription().getVersion() + " is now Enabled!");
		
	}
	
	@Override
	public void onDisable(){

		System.out.println("<<Info Command>> Version " + this.getDescription().getVersion() + " is now Disabled!");
		
	}

	public void checkFiles() {
		File fold = new File("plugins/InfoCommand/Commands");
		
		if(!fold.exists()){
			fold.mkdir();
		}
		
		for(String element : commands){
			File cmd = new File("plugins/InfoCommand/Commands/" + element + ".yml");
			if(!cmd.exists()){
				try {
					cmd.createNewFile();
					YamlConfiguration config = YamlConfiguration.loadConfiguration(cmd);
					config.set("Line.1", "This is an example of &4how to write &7each line!");
					config.set("Line.2", "Use minecraft color codes to change &2Color");
					config.save(cmd);
					
					System.out.println("<<Info Command>> Default File created for " + element);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			YamlConfiguration config = YamlConfiguration.loadConfiguration(cmd);
			
			Object[] temp = config.getConfigurationSection("Line").getKeys(true).toArray();

			String[] resp = new String[temp.length];
			for(int i=0; i<temp.length; i++){
				resp[i] = config.getString("Line." + (i+1));
				resp[i] = changeColor(resp[i]);
			}
			
			cmdResp.put(element, resp);
			
			System.out.println("<<Info Command>> Command Created for: " + element);
		}
	}

	private String changeColor(String string) {
		string = string.replaceAll("&0", ChatColor.BLACK + "");
		string = string.replaceAll("&1", ChatColor.DARK_BLUE + "");
		string = string.replaceAll("&2", ChatColor.DARK_GREEN + "");
		string = string.replaceAll("&3", ChatColor.DARK_AQUA + "");
		string = string.replaceAll("&4", ChatColor.DARK_RED + "");
		string = string.replaceAll("&5", ChatColor.DARK_PURPLE + "");
		string = string.replaceAll("&6", ChatColor.GOLD + "");
		string = string.replaceAll("&7", ChatColor.GRAY + "");
		string = string.replaceAll("&8", ChatColor.DARK_GRAY+ "");
		string = string.replaceAll("&9", ChatColor.BLUE + "");
		string = string.replaceAll("&a", ChatColor.GREEN + "");
		string = string.replaceAll("&b", ChatColor.AQUA + "");
		string = string.replaceAll("&c", ChatColor.RED + "");
		string = string.replaceAll("&d", ChatColor.LIGHT_PURPLE + "");
		string = string.replaceAll("&e", ChatColor.YELLOW + "");
		string = string.replaceAll("&f", ChatColor.WHITE + "");
		string = string.replaceAll("&g", ChatColor.MAGIC + "");
		return string;
	}

	public void getCmds() {

		Object[] temp = this.getConfig().getConfigurationSection("Commands").getKeys(true).toArray();
		commands = new String[temp.length];
		
		for(int i=0; i < temp.length; i++){
			commands[i] = this.getConfig().getString("Commands." + (i+1));
		}
		
	}

	@EventHandler
	public void onPreCmd(PlayerCommandPreprocessEvent event){
		String command = null;
		for(String element : commands){
			String i = "/" + element;
			if(i.equalsIgnoreCase(event.getMessage())){
				command = element;
			}
		}
		if(command != null){
			Player player = event.getPlayer();
			if(player.hasPermission("IC.use") || player.isOp()){
				for(String element : cmdResp.get(command)){
					player.sendMessage(element);
				}
			}
			event.setCancelled(true);
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("ic")){
				if(player.hasPermission("ic.admin") || player.isOp()){
					if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
							InfoCommand.plugin.getCmds();
							InfoCommand.plugin.checkFiles();
							System.out.println("<<Info Command>> Reloaded!");
							player.sendMessage(ChatColor.RED + "<<" + ChatColor.AQUA + "Info Command" + ChatColor.RED + ">>" + ChatColor.WHITE + " Reloaded!");
					}else{
						player.sendMessage(ChatColor.RED + "<<" + ChatColor.AQUA + "Info Command" + ChatColor.RED + ">>" + ChatColor.WHITE + " by Alvarez");
						player.sendMessage(ChatColor.RED + "<<" + ChatColor.AQUA + "Info Command" + ChatColor.RED + ">>" + ChatColor.WHITE + " To reload type: " + ChatColor.GOLD + "/ic reload");
					}
				}else{
					player.sendMessage(ChatColor.RED + "<<" + ChatColor.AQUA + "Info Command" + ChatColor.RED + ">>" + ChatColor.WHITE + " To use this command you need " + ChatColor.RED + "OP" + ChatColor.WHITE + " or " + ChatColor.RED + "IC.admin");
				}
			}
		}else{
			if(cmd.getName().equalsIgnoreCase("ic")){
				if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
					InfoCommand.plugin.getCmds();
					InfoCommand.plugin.checkFiles();
					System.out.println("<<Info Command>> Reloaded!");
				}else{
					System.out.println("<<Info Command>> To reload type: ic reload");
				}
			}
		}
		return true;
	}
}
